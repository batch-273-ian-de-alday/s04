// alert("test")

class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;

        if (grades.length === 4) {
            if (grades.every(grade => grade >= 0 && grade <= 100)) {
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }
    }

    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }
    logout() {
        console.log(`${this.email} has logged out`);
        return this
    }
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum / 4;
        return this;
    }
    willPass() {
        this.passed = this.computeAve().gradeAve >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {
        if (this.passed) {
            if (this.gradeAve >= 90) {
                this.passedWithHonors = true;
            } else {
                this.passedWithHonors = false;
            }
        } else {
            this.passedWithHonors = false;
        }
        return this;
    }
}



// Class to represent a section of a student
class Section {
    // every Section object will be instantiated with an empty array for its students
    constructor(name) {
        this.name = name;
        this.students = [];
        this.honorStudents = undefined;
        this.honorsPercentage = undefined;
    }

    // Method for adding student to this section
    // This will take in the same arguments needed to instantiate a new Student object
    addStudent(name, email, grades) {
        // a Student object will be instantiated before being pushed into the students array
        this.students.push(new Student(name, email, grades));

        // return this to allow for chaining
        return this;
    }

    // Method for computing the honor students in this section
    countHonorStudents() {
        let count = 0;
        this.students.forEach(student => {
            if (student.willPass().willPassWithHonors().passedWithHonors) {
                count++;
            }
        })
        this.honorStudents = count;
        return this;
    }

    // Method for computing the percentage of honor students in this section
    computeHonorsPercentage() {
        this.honorsPercentage = (this.countHonorStudents().honorStudents / this.students.length) * 100;
        return this;
    }
}

const section1A = new Section("Section1A");
const section1B = new Section("Section1B");
const section1C = new Section("Section1C");
const section1D = new Section("Section1D");

section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);


section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

// // section1A.addStudent("John", "john@mail.com", [90, 90, 90, 90]);
// // section1A.students[0].willPass().willPassWithHonors()
// // console.log(section1A);

// section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88])
// section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85])
// section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93])
// section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93])
// // section1A.countHonorStudents();
// section1A.computeHonorsPercentage();
// console.log(section1A);


// Mini - Activity:
/* 
    Create a method that is replica of computeHonorsPercentage()
    - create a propperty called honorsPercentage with a value of undefined
    - create a method called computeHonorsPercentage() that will compute for the totoal number of honor students divided by total number of students multiplied by 100
         - resulting value will be assigned to honorsPercentage
         - return the instance/object
*/


// Activity:

/* 1. Define a Grade class whose constructor will accept a number
argument to serve as its grade level. It will have the following
properties:
● level initialized to passed in number argument
● sections initialized to an empty array
● totalStudents initialized to zero
● totalHonorStudents initialized to zero
● batchAveGrade set to undefined
● batchMinGrade set to undefined
● batchMaxGrade set to undefined */


class Grade {
    constructor(level) {
        this.level = level;
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }

    // Method for adding the created section to the sections array
    addSection(section) {
        this.sections.push(section);
        return this;
    }

    /*  Define a countStudents() method that will iterate over every section in the grade level, incrementing the totalStudents property of the grade level object for every student found in every section.*/
    countStudents() {
        this.sections.forEach(section => {
            this.totalStudents += section.students.length;
        })
        return this;
    }

    /* Define a countHonorStudents() method that will perform similarly to countStudents() except that it will only consider honor students when incrementing the totalHonorStudents property. */
    countHonorStudents() {
        this.sections.forEach(section => {
            this.totalHonorStudents += section.honorStudents;
        })
        return this;
    }
    /* Define a computeBatchAve() method that will get the average of all the students' grade averages and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation. */
    computeBatchAve() {
        let total = 0;
        this.sections.forEach(section =>
            section.students.forEach(student => {
                total += student.gradeAve;
            })
        )
        this.batchAveGrade = total / this.totalStudents;
        return this;
    }

    /* Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student of this grade level regardless of section. */
    getBatchMinGrade() {
        let min = 100;
        this.sections.forEach(section =>
            section.students.forEach(student => {
                student.grades.forEach(grade => {
                    if (grade < min) {
                        min = grade;
                    }
                })
            })
        )
        this.batchMinGrade = min;
        return this;
    }

    /* Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade scored by a student of this grade level regardless of section. */
    getBatchMaxGrade() {
        let max = 0;
        this.sections.forEach(section =>
            section.students.forEach(student => {
                student.grades.forEach(grade => {
                    if (grade > max) {
                        max = grade;
                    }
                })
            })
        )
        this.batchMaxGrade = max;
        return this;
    }
}

const grade1 = new Grade(1);

//2. add sections to this grade level

grade1.addSection(section1A);
grade1.addSection(section1B);
grade1.addSection(section1C);
grade1.addSection(section1D);

//4. count the total number of students in this grade level
grade1.countStudents()

//5. count the total number of honor students in this grade level
section1A.countHonorStudents().computeHonorsPercentage()
section1B.countHonorStudents().computeHonorsPercentage()
section1C.countHonorStudents().computeHonorsPercentage()
section1D.countHonorStudents().computeHonorsPercentage()
grade1.countHonorStudents()

//6. compute the average grade of the grade level
grade1.computeBatchAve()

//7. compute the minimum grade of the grade level
grade1.getBatchMinGrade()

//8. compute the maximum grade of the grade level
grade1.getBatchMaxGrade()

console.log(section1A)
console.log(section1B)
console.log(section1C)
console.log(section1D)
console.log(grade1);






